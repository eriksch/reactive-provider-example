module.exports = {
  publicPath: process.env.NODE_ENV === 'production'
    ? '/reactive-provider-example/'
    : '/',
  chainWebpack: (config) => {
    config.module
      .rule("vue")
      .use("vue-loader")
      .loader("vue-loader")
      .tap((options) => {
        options.transpileOptions = {
          transforms: {
            dangerousTaggedTemplateString: true,
          },
        };
        return options;
      });
  },
};
